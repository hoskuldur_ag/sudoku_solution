import { Component } from '@angular/core';
import * as $ from 'jquery';
import { stripSummaryForJitFileSuffix } from '@angular/compiler/src/aot/util';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  title = 'Sudoku Solver!';

  // Runs through each character in the submitted puzzle and adds it to the corresponding cell.
  fillSudokuPuzzle() {
    let digit = '';
    const submittedPuzzle = $('#puzzle-input').val().toString();

    for (let i = 0; i < submittedPuzzle.length; i++) {
      digit = $('#puzzle-input').val().toString().charAt(i);

      if (digit !== '.') {
        $('#cell-' + i).val(digit);
      }
    }
  }

  // Runs through each cell in the puzzle and adds all possible candidates to it.
  addCandidatesToCells() {
    let cellRow = null;
    let cellCol = null;
    let cellBox = null;

    for (let i = 0; i < 81; i++) {
      // If a cell has only one digit, lock it in with a 'Solved' class.
      // Will be used in future func calls to ignore all cells with single digits.
      if ($('#cell-' + i).val().toString() !== '') {
        $('#cell-' + i).addClass('solved');
      }

      if ($('#cell-' + i).val().toString() === '') {
        cellRow = $('#cell-' + i).attr('class').split(' ')[1];
        cellCol = $('#cell-' + i).attr('class').split(' ')[2];
        cellBox = $('#cell-' + i).attr('class').split(' ')[3];

        for (let possibleCandidate = 1; possibleCandidate <= 9; possibleCandidate++) {
          if (this.isValueInUnits(possibleCandidate, cellRow, cellCol, cellBox) === false) {

            // Add value to Candidates.
            $('#cell-' + i).val($('#cell-' + i).val().toString() + possibleCandidate);
          }

          // Else: Nothing happens.

        }
      }
    }
  }

  // Checks if a certain digit can be found in the same row as the empty cell.
  isValueInUnits(candidate, cellRow, cellCol, cellBox) {
    const rowArray = $('.' + cellRow);
    const colArray = $('.' + cellCol);
    const boxArray = $('.' + cellBox);

    // If any cell in the row has the candidate as a digit: Return true.
    for (let i = 0; i < rowArray.length; i++) {
      const valueInCell = (<HTMLInputElement>rowArray[i]).value;
      // Found no other way to compare the two, probably a hazard. Ask.
       if (valueInCell == candidate) {
         return true;
       }
    }

    // If any cell in the column has the candidate as a digit: Return true.
    for (let i = 0; i < colArray.length; i++) {
      const valueInCell = (<HTMLInputElement>colArray[i]).value;
      // Found no other way to compare the two, probably a hazard. Ask.
       if (valueInCell == candidate) {
         return true;
       }
    }

    // If any cell in the box has the candidate as a digit: Return true.
    for (let i = 0; i < boxArray.length; i++) {
      const valueInCell = (<HTMLInputElement>boxArray[i]).value;
      // Found no other way to compare the two, probably a hazard. Ask.
       if (valueInCell == candidate) {
         return true;
       }
    }

    // Otherwise return false.
    return false;
  }

  stringTest() {
    const cellID = '#cell-3';
    const digit = 9;
    if ($(cellID).val().toString().includes(digit.toString())) {
      console.log($(cellID).val());
      const valueString = $(cellID).val().toString();
      const indexOfDigit = valueString.indexOf(digit.toString());

      const firstHalf = $(cellID).val().toString().slice(0, indexOfDigit);
      const secondHalf = $(cellID).val().toString().slice(indexOfDigit + 1);
      $(cellID).val(firstHalf + secondHalf);
      console.log($(cellID).val());
    }
  }

  removeRedundantCandidatesFromCells() {
    let cellRow = null;
    let cellCol = null;
    let cellBox = null;

    for (let i = 0; i < 81; i++) {
      // If a cell has a 'Solved' class, ignore it. (See addCandidatesToCells() function).
      const hasSolvedClass = $('#cell-' + i).hasClass('solved');
      if (!hasSolvedClass) {

        // If a cell has only one digit, lock it in with a 'Solved' class.
        // Will be used in future func calls to ignore all cells with single digits.
        // Also, remove the digit as a candidate from all cells in the same row, column and box.
        if ($('#cell-' + i).val().toString().length === 1) {
          $('#cell-' + i).addClass('solved');

          const digit = $('#cell-' + i).val();
          cellRow = $('#cell-' + i).attr('class').split(' ')[1];
          cellCol = $('#cell-' + i).attr('class').split(' ')[2];
          cellBox = $('#cell-' + i).attr('class').split(' ')[3];
          const rowArray = $('.' + cellRow);
          const colArray = $('.' + cellCol);
          const boxArray = $('.' + cellBox);

          // Remove digit from row.
          for (let x = 0; x < rowArray.length; x++) {
            const cellValue = (<HTMLInputElement>rowArray[x]).value;
            const cellClasses = (<HTMLInputElement>rowArray[x]).className;
            const cellID = (<HTMLInputElement>rowArray[x]).id;

            if (cellValue.includes(digit.toString()) && !cellClasses.includes('solved')) {
              const valueString = $('#' + cellID).val().toString();
              const indexOfDigit = valueString.indexOf(digit.toString());

              const firstHalf = $('#' + cellID).val().toString().slice(0, indexOfDigit);
              const secondHalf = $('#' + cellID).val().toString().slice(indexOfDigit + 1);

              $('#' + cellID).val(firstHalf + secondHalf);
            }
          }

          // Remove digit from column.
          for (let x = 0; x < colArray.length; x++) {
            const cellValue = (<HTMLInputElement>colArray[x]).value;
            const cellClasses = (<HTMLInputElement>colArray[x]).className;
            const cellID = (<HTMLInputElement>colArray[x]).id;

            if (cellValue.includes(digit.toString()) && !cellClasses.includes('solved')) {
              const valueString = $('#' + cellID).val().toString();
              const indexOfDigit = valueString.indexOf(digit.toString());

              const firstHalf = $('#' + cellID).val().toString().slice(0, indexOfDigit);
              const secondHalf = $('#' + cellID).val().toString().slice(indexOfDigit + 1);

              $('#' + cellID).val(firstHalf + secondHalf);
            }
          }

          // Remove digit from box.
          for (let x = 0; x < boxArray.length; x++) {
            const cellValue = (<HTMLInputElement>boxArray[x]).value;
            const cellClasses = (<HTMLInputElement>boxArray[x]).className;
            const cellID = (<HTMLInputElement>boxArray[x]).id;

            if (cellValue.includes(digit.toString()) && !cellClasses.includes('solved')) {
              const valueString = $('#' + cellID).val().toString();
              const indexOfDigit = valueString.indexOf(digit.toString());

              const firstHalf = $('#' + cellID).val().toString().slice(0, indexOfDigit);
              const secondHalf = $('#' + cellID).val().toString().slice(indexOfDigit + 1);

              $('#' + cellID).val(firstHalf + secondHalf);
            }
          }

        }

      }
    }
  }
}
